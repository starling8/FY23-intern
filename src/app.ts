import { DataSource } from 'typeorm'
import Express from 'express'
import * as bodyParser from 'body-parser'
import { UserService } from './services/userService'
import { User } from './entity/user'

export function createApp(dataSource: DataSource) {
  const app = Express()
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())

  const service = new UserService(dataSource.getRepository(User))

  app.get('/', (_, res) => {
    res.json({ message: 'hello' })
  })

  app.get('/users', async (_, res) => {
    const users = await service.getAll()
    res.json(users)
  })

  app.put('/users/:id', async (req, res) => {
    
    const [result, resultId] = tryParse(req.params.id)

    if (result == false){
      res.status(400).json({ message: 'Invalid id specified' })
      return
    }
    const  { firstName, lastName, age, school } = req.body
    const existingUser: User = await service.findOne(resultId)
    if(!existingUser){
      res.status(404).json({ message: 'User not found.' })
      return
    }
    existingUser.firstName = firstName
    existingUser.lastName = lastName
    existingUser.age = age
    existingUser.school = school
    const updateUser = await service.update(existingUser)
    res.json(updateUser)
  })

  app.get('/users/:id', async (req, res) => {

    const [result, resultId] = tryParse(req.params.id)

    if (result == false){
      res.status(400).json({ message: 'Invalid id specified' })
      return
    }
    const user = await service.findOne(resultId)
    if(user){
      res.json(user)
      return
    }
    res.status(404).json({message : 'User not found'})
  })

  app.post('/users', async (req, res) => {
    const  { firstName, lastName, age, school } = req.body
    const user: User = new User()
    user.firstName = firstName, user.lastName = lastName, user.age = age, user.school = school
    await service.create(user)
    res.json(user)
  })

  app.delete('/users/:id', async (req, res) => {

    const [result, resultId] = tryParse(req.params.id)
    
    if (result == false){
      res.status(400).json({ message: 'Invalid id specified' })
      return
    }
    const user: User = await service.findOne(resultId)
    if(!user) {
      res.status(404).json({ message: `User id:${resultId} is Not Found.` })
      return
    }
    await service.removeOne(user)
    res.json({ message: `deleted id:${resultId} user.` })
  })

  app.delete('/users', async (_, res) => {
    await service.removeAll()
    res.json({ message: 'deleted All users.' })
  })

  function tryParse(value): [boolean, number | null] {
    let result: boolean = false
    const resultId = parseInt(value, 10)
    if(isNaN(resultId)) return [result, null]

    result = true
    return [result, resultId]
  }
  return app

}