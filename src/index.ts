import { createApp } from './app'
import { initDataSource } from './data-source'

initDataSource()
  .then((dataSource) => {
    const port = 3000
    const app = createApp(dataSource)
    app.listen(port)
    console.log(`API running on localhost:${port}`)
  })
  .catch((e) => {
    console.error('failed to start server')
    console.error(e)
    process.exit(-1)
  })
