import 'reflect-metadata'
import { DataSource, DataSourceOptions } from 'typeorm'

const dbConfig: DataSourceOptions = {
  type: 'sqlite',
  database: 'data/dev.sqlite',
  synchronize: false,
  logging: false,
  entities: [__dirname + '/entity/*.ts'],
  migrations: [__dirname + '/migration/*.ts'],
  migrationsTableName: 'migrations',
  subscribers: [],
}

export const AppDataSource = new DataSource(dbConfig)

export async function initDataSource() {
  const appDataSource = new DataSource(dbConfig)
  await appDataSource.initialize()
  return appDataSource
}
