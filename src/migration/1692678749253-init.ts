import { MigrationInterface, QueryRunner } from 'typeorm'

export class init1692678749253 implements MigrationInterface {
  name = 'init1692678749253'

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "firstName" text NOT NULL, "lastName" text NOT NULL, "school" text NOT NULL, "age" integer NOT NULL)`,
    )
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "user"`)
  }
}
