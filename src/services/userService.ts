import { Repository } from 'typeorm'
import { User } from '../entity/user'

export class UserService {
  constructor(private repository: Repository<User>) {}

  async getAll(): Promise<User[]> {
    return this.repository.find()
  }

  async create(user: User): Promise<User> {
    return this.repository.save(user)
  }

  async findOne(id: number): Promise<User | null> {
    return this.repository.findOneBy({ id })
  }

  async update(user: User): Promise<User> {
    return this.repository.save(user)
  }

  async removeOne(user: User): Promise<void> {
    await this.repository.remove(user)
  }

  async removeAll(): Promise<void> {
    await this.repository.clear()
  }
}
