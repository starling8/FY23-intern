import { DataSource } from 'typeorm'
import { initDataSource } from '../src/data-source'
import request from 'supertest'
import { createApp } from '../src/app'
import { UserService } from '../src/services/userService'
import { User } from '../src/entity/user'

let dataSource: DataSource

beforeAll(async () => {
    dataSource = await initDataSource()
})

afterEach(async () => {
    const service = new UserService(dataSource.getRepository(User))
    await service.removeAll()
})

describe('POST /users', () => {
    it('create a user with json', async () => {
        let user1 = new User()
        user1.firstName = 'Jirokichi'
        user1.lastName = 'Suzuki'
        user1.age = 72
        user1.school = 'Toto University'
        
        const response = await request(createApp(dataSource)).post('/users').send(user1)
        expect(response.status).toBe(200)
        expect(response.body).toMatchObject(user1)
    })
})