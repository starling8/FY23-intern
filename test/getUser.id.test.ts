import { DataSource } from 'typeorm'
import { initDataSource } from '../src/data-source'
import request from 'supertest'
import { createApp } from '../src/app'
import { UserService } from '../src/services/userService'
import { User } from '../src/entity/user'

let dataSource: DataSource

beforeAll(async () => {
    dataSource = await initDataSource()
})

afterEach(async () => {
    const service = new UserService(dataSource.getRepository(User))
    await service.removeAll()
})

describe('GET /users/:id', () => {
    it('get a user with id (success)', async () => {
        let user1 = new User()
        user1.firstName = 'Jirokichi'
        user1.lastName = 'Suzuki'
        user1.age = 72
        user1.school = 'Toto University'

        const service = new UserService(dataSource.getRepository(User))
        const createdUser = await service.create(user1)
        
        const response = await request(createApp(dataSource)).get(`/users/${createdUser.id}`)

        expect(response.status).toBe(200)
        expect(response.body).toMatchObject(user1)
    })
    it('get a user with id (failure)', async () => {
        let user1 = new User()
        user1.firstName = 'Jirokichi'
        user1.lastName = 'Suzuki'
        user1.age = 72
        user1.school = 'Toto University'

        const service = new UserService(dataSource.getRepository(User))
        const createdUser = await service.create(user1)

        const response = await request(createApp(dataSource)).get(`/users/${createdUser.id + 1}`)

        const expected = {message : 'User not found'}

        expect(response.status).toBe(404)
        expect(response.body).toMatchObject(expected)
    })
    it('get a user with id (string)', async () => {
        let user1 = new User()
        user1.firstName = 'Jirokichi'
        user1.lastName = 'Suzuki'
        user1.age = 72
        user1.school = 'Toto University'

        const service = new UserService(dataSource.getRepository(User))
        const createdUser = await service.create(user1)

        const expected = { message: 'Invalid id specified' }

        const response1 = await request(createApp(dataSource)).get(`/users/m0jiretsudesu`)
        expect(response1.status).toBe(400)
        expect(response1.body).toMatchObject(expected)
    })
})