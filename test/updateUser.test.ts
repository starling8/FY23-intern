import { DataSource } from 'typeorm'
import { initDataSource } from '../src/data-source'
import request from 'supertest'
import { createApp } from '../src/app'
import { UserService } from '../src/services/userService'
import { User } from '../src/entity/user'

let dataSource: DataSource

beforeAll(async () => {
    dataSource = await initDataSource()
})

afterEach(async () => {
    const service = new UserService(dataSource.getRepository(User))
    await service.removeAll()
})

describe('PUT /users/:id', () => {
    it('update a user(success)', async () => {
        const service = new UserService(dataSource.getRepository(User))
        let user1 = new User()
        user1.firstName = 'Asahi'
        user1.lastName = 'Takahashi'
        user1.age = 21
        user1.school = 'Yamaguchi University'
        user1 = await service.create(user1)
        user1.firstName = 'Taro'
        const response = await request(createApp(dataSource)).put(`/users/${user1.id}`).send(user1)
        expect(response.status).toBe(200)
        expect(response.body).toMatchObject(user1)
        expect(response.body).toMatchObject(await service.findOne(user1.id))
    })
    it('update a user(fail)', async () => {
        const service = new UserService(dataSource.getRepository(User))
        let user1 = new User()
        user1.firstName = 'Asahi'
        user1.lastName = 'Takahashi'
        user1.age = 21
        user1.school = 'Yamaguchi University'
        user1 = await service.create(user1)
        const response = await request(createApp(dataSource)).put(`/users/${user1.id+1}`).send(user1)
        expect(response.status).toBe(404)
        expect(response.body).toMatchObject({ message: 'User not found.' })
    })
})