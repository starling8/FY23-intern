import { DataSource } from 'typeorm'
import { initDataSource } from '../src/data-source'
import request from 'supertest'
import { createApp } from '../src/app'
import { UserService } from '../src/services/userService'
import { User } from '../src/entity/user'

let dataSource: DataSource

beforeAll(async () => {
  dataSource = await initDataSource()
})

afterEach(async () => {
  const service = new UserService(dataSource.getRepository(User))
  await service.removeAll()
})

describe('GET /users', () => {
  it('return all users', async () => {
    const service = new UserService(dataSource.getRepository(User))
    let user1 = new User()
    user1.firstName = 'Taro'
    user1.lastName = 'Tanaka'
    user1.age = 22
    user1.school = 'Shinshu University'
    user1 = await service.create(user1)
    const response = await request(createApp(dataSource)).get('/users')
    expect(response.status).toBe(200)
    expect(response.body).toMatchObject([user1])
  })
})
