import { DataSource } from 'typeorm'
import { initDataSource } from '../src/data-source'
import request from 'supertest'
import { createApp } from '../src/app'
import { UserService } from '../src/services/userService'
import { User } from '../src/entity/user'

let dataSource: DataSource

beforeAll(async () => {
  dataSource = await initDataSource()
})

afterEach(async () => {
  const service = new UserService(dataSource.getRepository(User))
  await service.removeAll()
})

describe('DELETE /users/id', () => {
  it('delete a user success', async () => {
    const service = new UserService(dataSource.getRepository(User))
    let user1 = new User()
    user1.firstName = 'Taro'
    user1.lastName = 'Tanaka'
    user1.age = 22
    user1.school = 'Shinshu University'
    user1 = await service.create(user1)
    const response = await request(createApp(dataSource)).delete(`/users/${user1.id}`)
    expect(response.body).toMatchObject({ message: `deleted id:${user1.id} user.` })
    const result = await service.findOne(user1.id)
    expect(result).toBeNull()
  })
  it('delete a user  fail', async () => {
    const service = new UserService(dataSource.getRepository(User))
    let user1 = new User()
    user1.firstName = 'Taro'
    user1.lastName = 'Tanaka'
    user1.age = 22
    user1.school = 'Shinshu University'
    user1 = await service.create(user1)
    const response = await request(createApp(dataSource)).delete(`/users/${user1.id+1}`)
    expect(response.status).toBe(404)
    expect(response.body).toMatchObject({ message: `User id:${user1.id+1} is Not Found.` })
    const result = await service.findOne(user1.id+1)
    expect(result).toBeNull()
  })
})